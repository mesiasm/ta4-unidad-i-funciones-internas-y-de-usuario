"""                         author: "Angel Morocho"
                    email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 1: Ejecuta el programa en tu sistema y observa qué números
                 obtienes."""

import random

print(random.randint(5, 10)) # Se obtiene un numero aleatorio entre 5 y 10
t = [1, 2, 3]
print(random.choice(t)) # Se obtiene un numero aleatorio de acuerdo a la variable
