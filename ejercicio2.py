"""                        author: "Angel Morocho"
                    email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 2: Desplaza la última línea del programa anterior hacia arriba,
             de modo que la llamada a la función aparezca antes que las definiciones.
             Ejecuta el programa y observa qué mensaje de error obtienes."""

repite_estribillo() # Error, referencia no resuelta debido a la no declaracion de funciones.


def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()