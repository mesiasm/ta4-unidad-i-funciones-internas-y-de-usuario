"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 7: Reescribe el programa de calificaciones del capítulo anterior usando una función
             llamada calcula_calificacion, que reciba una puntuación como parámetro y devuelva
             una calificación como cadena."""


def calcula_calificacion(calif):
    if 0 <= calif <= 1:
        if calif >= 0.9:
            print('Sobresaliente')
        elif calif >= 0.8:
            print('Notable')
        elif calif >= 0.7:
            print('Bien')
        elif calif >= 0.6:
            print('Suficiente')
        elif calif < 0.6:
            print(' Insuficiente')
    else:
        print('Puntuacion incorrecta')
    return calif


if __name__ == '__main__':
    while True:
        try:
            cal = float(input('Introduzca la puntuación '))
            print(calcula_calificacion(cal))
            break
        except ValueError:
            print('Puntuacion incorrecta, ingrese numeros')