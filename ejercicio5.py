"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 5: ¿Qué mostrará en pantalla el siguiente programa Python?"""

def fred():
    print("Zap")

def jane():
    print("ABC")

jane()
fred()
jane()
# Presenta: ABC, Zap, ABC (Literal D)

