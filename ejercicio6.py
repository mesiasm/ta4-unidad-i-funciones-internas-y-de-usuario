"""               author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 6: Reescribe el programa de cálculo del salario, con tarifa-y media para las horas
             extras, y crea una función llamada calculo_salario que reciba dos parámetros
             (horas y tarifa)."""


def calculo_salario(horas, tarifa):
    if horas >= 40:
        horas_xt = horas - 40
        tarifa_xt = (0.5 * tarifa) + tarifa
        salario = (tarifa_xt * horas_xt) + (horas * tarifa)
    else:
        salario = tarifa * horas
    return salario


if __name__ == '__main__':
    while True:
        try:
            horas = int(input("ingrese el numero de horas laboradas: \n"))
            while True:
                try:
                    tarifa = float(input("ingrese la tarifa por hora: \n "))
                    salario = calculo_salario(horas, tarifa)
                    print('Su salario es de: ', salario)
                    break
                except ValueError:
                    print("Tarifa incorrecta: Ingrese numeros")
            break
        except ValueError:
            print("Hora incorrecta: Ingrese numeros")