"""                          author: "Angel Morocho"
                       e-mail: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 3: Desplaza la llamada de la función de nuevo hacia el final, y coloca la definición
             de muestra_estribillo después de la definición de repite_estribillo. ¿Qué ocurre
             cuando haces funcionar ese programa?"""

def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()

repite_estribillo()
print('Soy un leñador, que alegría.')
print('Duermo toda la noche y trabajo todo el día.')

# Se presentan los mensajes de la funcion repite estribillo anidada a la funcion muestra estribillo
# y las sentencias de la funcion muestra estribillo sin llamar a la funcion.
